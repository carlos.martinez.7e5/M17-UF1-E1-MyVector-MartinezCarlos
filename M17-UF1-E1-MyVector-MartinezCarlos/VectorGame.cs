﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_MartinezCarlos
{
    public class VectorGame
    {
        //Genera un array de MyVector con puntos aleatorios
        public MyVector[] randomVectors(int numVecs)
        {
            //Crea un array de objetos MyVector con tantos espacios como el parámetro que se le pasa
            var vecList = new MyVector[numVecs];

            //Por cada espacio de MyVector...
            for (int i = 0; i < numVecs; i++)
            {
                //Genera un array de numeros auxiliar con 4 espacios
                var auxVec = new int[4];

                //Se rellena aleatoriamente
                for (int j = 0; j < 4; j++)
                {
                    auxVec[0] = randomNum();
                    auxVec[1] = randomNum();
                    auxVec[2] = randomNum();
                    auxVec[3] = randomNum();
                }

                //Se crea un objeto con el vector de numeros aleatorios
                vecList[i] = new MyVector(auxVec[0], auxVec[1], auxVec[2], auxVec[3]);
            }
            return vecList;
        }

        public MyVector[] sortVectors(MyVector[] arrayVectors, bool metodoOrdenacion)
        {
            for (int i = 0; i < arrayVectors.Length - 1; i++)
            {
                for (int j = i + 1; j < arrayVectors.Length; j++)
                {
                    if (metodoOrdenacion) comparaLongitud(ref arrayVectors[i], ref arrayVectors[j]);
                    else comparaDistanciaCentro(ref arrayVectors[i], ref arrayVectors[j]);
                }
            }
            return arrayVectors;
        }

        private void comparaLongitud(ref MyVector vA, ref MyVector vB)
        {
            if (vA.longitudVector() > vB.longitudVector()) (vA, vB) = (vB, vA);
        }

        private void comparaDistanciaCentro(ref MyVector vA, ref MyVector vB)
        {
            if (vA.distanciaDelCentre() > vB.distanciaDelCentre()) (vA, vB) = (vB, vA);
        }

        private int randomNum()
        {
            var r = new Random();
            return r.Next(0, 21);
        }

        public void writeVector(MyVector[] vec)
        {
            foreach (MyVector v in vec) { Console.WriteLine(v); }
        }
    }
}
