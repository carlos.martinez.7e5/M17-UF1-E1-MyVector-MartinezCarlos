﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_MartinezCarlos
{
    class Ejercicios
    {
        public static void Ej1()
        {
            Console.Clear();

            //Ejercicio 1. Pedir un nombre por pantalla y mostrar un saludo.
            Console.Write("Cómo te llamas? ");
            var nombre = Console.ReadLine();
            Utiles.colorText("Hello " + nombre, ConsoleColor.Green);
            Console.ReadKey();
        }

        public static void Ej2()
        {
            Console.Clear();

            //Ejercicio 2. Genera un array de tantos vectores aleatorios como numero le pase el usuario como parametro.
            var vg = new VectorGame();
            int numVecs;
            do
            {
                Console.Write("Cuantos vectores aleatorios quieres generar? ");
                numVecs = Convert.ToInt32(Console.ReadLine());
                if (numVecs <= 0) Utiles.colorText("ERROR: Numero no válido, introduce un numero positivo o mayor que 0", ConsoleColor.Red);
            } while (numVecs <= 0);

            var vRandom = vg.randomVectors(numVecs);
            
            Console.ForegroundColor = ConsoleColor.Yellow;
            vg.writeVector(vRandom);
            Console.ResetColor();

            Console.ReadKey();
        }

        public static void Ej3()
        {
            Console.Clear();

            //Ejercicio 3. A partir de un array de vectores, los ordena por longitud o por cercania al centro.
            int choice;
            var vg = new VectorGame();

            do
            {
                Console.WriteLine("Quieres generar un array aleatorio o introducirlo manualmente?");
                Utiles.colorText("[1] Aleatorio", ConsoleColor.Blue);
                Utiles.colorText("[2] Manual", ConsoleColor.Blue);

                choice = Convert.ToInt32(Console.ReadLine());
                if (choice != 1 && choice != 2) Utiles.colorText("ERROR: Opción no valida. Elige entre las opciones disponibles.", ConsoleColor.Red);
            } while (choice != 1 && choice != 2);

            int numVecs;
            do
            {
                Console.Write("De que longitud será el array?: ");
                numVecs = Convert.ToInt32(Console.ReadLine());
                if (numVecs <= 0) Utiles.colorText("ERROR: Numero no válido, introduce un numero positivo o mayor que 0", ConsoleColor.Red);
            } while (numVecs <= 0);

            var vectorArray = new MyVector[numVecs];
            if(choice == 1)
            {
                vectorArray = vg.randomVectors(numVecs);
            }
            else
            {
                for(int i = 0; i < vectorArray.Length; i++)
                {
                    Console.WriteLine("Vector " + (i+1));
                    Console.Write("iX = ");
                    var aux1 = Convert.ToInt32(Console.ReadLine());
                    Console.Write("iY = ");
                    var aux2 = Convert.ToInt32(Console.ReadLine());
                    Console.Write("fX = ");
                    var aux3 = Convert.ToInt32(Console.ReadLine());
                    Console.Write("fY = ");
                    var aux4 = Convert.ToInt32(Console.ReadLine());

                    var auxVector = new MyVector(aux1, aux2, aux3, aux4);
                    vectorArray[i] = auxVector;
                }
            }

            do
            {
                Console.WriteLine("Como lo quieres ordenar?: ");
                Utiles.colorText("[1] Por longitud", ConsoleColor.Blue);
                Utiles.colorText("[2] Por proximidad al 0,0", ConsoleColor.Blue);

                choice = Convert.ToInt32(Console.ReadLine());
                if (choice != 1 && choice != 2) Utiles.colorText("ERROR: Opción no valida. Elige entre las opciones disponibles.", ConsoleColor.Red);
            } while (choice != 1 && choice != 2);

            if(choice == 1) { vg.sortVectors(vectorArray, true); } 
            else { vg.sortVectors(vectorArray, false); }

            Console.ForegroundColor = ConsoleColor.Blue;
            vg.writeVector(vectorArray);
            Console.ResetColor();

            Console.ReadKey();
        }
    }
}
