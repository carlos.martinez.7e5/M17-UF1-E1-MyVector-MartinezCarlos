﻿using System;

namespace M17_UF1_E1_MyVector_MartinezCarlos
{
    class Program
    {
        public static void Main()
        {
            int ej;
            do
            {
                Menu.listaEjercicios();
                ej = Convert.ToInt32(Console.ReadLine());
                Menu.eligeEjercicio(ej);   
            }
            while (ej != 0);
            Console.WriteLine("Adiós! ^-^");
            Console.ReadKey();
        }
    }
}