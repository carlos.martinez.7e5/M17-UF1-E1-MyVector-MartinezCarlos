﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_MartinezCarlos
{
    public class MyVector
    {
        //Los puntos del vector
        public int[] puntoInicio = new int[2]; //Punto de inicio con dos int (X e Y).
        public int[] puntoFinal = new int[2]; ////Punto final con dos int (X e Y).

        //Metodo constructor para crear el objeto MyVector.
        public MyVector(int x0, int y0, int x1, int y1)
        {
            puntoInicio[0] = x0;
            puntoInicio[1] = y0;
            puntoFinal[0] = x1;
            puntoFinal[1] = y1;
        }

        //Metodos para cambiar el valor de los puntos de Inicio y Final.
        public void SetInicio(int x, int y)
        {
            puntoInicio[0] = x;
            puntoInicio[1] = y;
        }

        public void SetFinal(int x, int y)
        {
            puntoFinal[0] = x;
            puntoFinal[1] = y;
        }

        //Para obtener el valor de los puntos de Inicio y Final.
        public int[] GetInicio() { return puntoInicio; }
        public int[] GetFinal() { return puntoFinal; }

        //Metodo para calcular la longitud del vector (la distancia entre los dos puntos).
        public double longitudVector()
        {
            //Calcula la diferencia entre las Xs y las Ys para obtener los "catetos" y hace pitágoras para encontrar la longitud del vector.
            var cateto1 = puntoInicio[0] - puntoFinal[0];
            var cateto2 = puntoInicio[1] - puntoFinal[1];

            return Math.Round(Math.Sqrt(Math.Pow(cateto1, 2) + Math.Pow(cateto2, 2)), 2); //Math.Round para que solo aparezcan 2 decimales.
        }

        //Similar al metodo anterior calcula la distancia entre dos puntos.
        //En este caso calcula dos distancias, ambos puntos respecto al 0,0 y devuelve el menor.
        public double distanciaDelCentre()
        {
            var cateto1 = puntoInicio[0];
            var cateto2 = puntoInicio[1];

            double distPinicio = Math.Round(Math.Sqrt(Math.Pow(cateto1, 2) + Math.Pow(cateto2, 2)), 2);

            //Se pueden reciclar las variables porque el resultado del otro punto ya está calculado.
            cateto1 = puntoFinal[0];
            cateto2 = puntoFinal[1];

            double distPfinal = Math.Round(Math.Sqrt(Math.Pow(cateto1, 2) + Math.Pow(cateto2, 2)), 2);

            if (distPinicio < distPfinal) return distPinicio;
            else return distPfinal;
        }

        //Invierte ambos puntos para cambiar el sentido del vector.
        public void CanviDireccio() { (puntoInicio, puntoFinal) = (puntoFinal, puntoInicio); }

        //ToString
        public override string ToString()
        {
            return "xI = " + puntoInicio[0] + " yI = " + puntoInicio[1] +
            ", xF = " + puntoFinal[0] + " yF = " + puntoFinal[1] + ". Longitud del vector: " +
            longitudVector() + ". Distancia del centro: " + distanciaDelCentre();
        }
    }
}
