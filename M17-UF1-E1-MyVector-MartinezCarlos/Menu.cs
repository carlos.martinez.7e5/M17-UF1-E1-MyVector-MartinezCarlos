﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_MartinezCarlos
{
    class Menu
    {
        public static void listaEjercicios()
        {
            Console.Clear();
            Console.WriteLine("Que ejercicio quieres ejecutar?: ");

            Utiles.colorText("[1] - Saludo", ConsoleColor.Green);
            Utiles.colorText("[2] - Generar vectores aleatorios", ConsoleColor.Yellow);
            Utiles.colorText("[3] - Ordenar array de vectores", ConsoleColor.Blue);
            Utiles.colorText("[0] - Salir", ConsoleColor.Red);
        }

        public static void eligeEjercicio(int ej)
        {
            switch (ej)
            {
                case 1:
                    {
                        Ejercicios.Ej1();
                        break;
                    }
                case 2:
                    {
                        Ejercicios.Ej2();
                        break;
                    }
                case 3:
                    {
                        Ejercicios.Ej3();
                        break;
                    }
                case 0:
                    {
                        break;
                    }
                default:
                    {
                        Utiles.colorText("Opción no valida", ConsoleColor.Red);
                        break;
                    }
            }
        }
    }
}
